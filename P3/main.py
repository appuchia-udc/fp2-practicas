#! /usr/bin/env python3

# Práctica 3 FP2: Listas posicionales ordenadas
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

from argparse import ArgumentParser
import sys

from models import Book

# Use the array-based positional list by default
from array_positional_list import ArrayPositionalList as PositionalList

# CONSTS
LINKED = False
MENU = """\
--------------- Menú ---------------
1) Leer fichero.
2) Mostrar media de préstamos.
3) Eliminar versiones duplicadas.
4) Buscar libros (submenú)

Q) Salir.\
"""
SEARCH_MENU = """\
------------- Búsqueda -------------
1) Buscar por título.
2) Buscar por autor.
3) Buscar por año.
4) Mostrar todos los libros.

Q) Volver al menú principal.\
"""


def get_books(path: str) -> PositionalList:
    """
    Reads the file and returns a list of tuples with the data.

    Each line of the file is a tuple with the following format:
    (author, title, year, times borrowed)

    Parameters
    ----------
    path : str
        The path to the file to read.

    Returns
    -------
    books : PositionalList
    """

    books = PositionalList()

    with open(path) as f:
        for line in f.readlines():
            title, author, year, times_borrowed = line.split(";")
            new_book = Book(author, title, int(year), int(times_borrowed))

            p = books.first()
            while True:
                if books.is_empty():
                    books.add_last(new_book)
                    break

                book = books.get_element(p)
                if p == books.last() and book <= new_book:
                    books.add_last(new_book)
                    break

                if book > new_book:
                    books.add_before(p, new_book)
                    break

                p = books.after(p)

    return books


def deduplicate(books: PositionalList) -> PositionalList:
    """Removes the duplicates from the list.

    As the books are sorted, the algorithm will only check the books
    directly before the current one.

    Parameters
    ----------
    books : PositionalList
        The list to remove the duplicates from.

    Returns
    -------
    books : PositionalList
        The list without duplicates.
    """

    p = books.first()

    while p != books.last():
        book = books.get_element(p)
        p_after = books.after(p)
        next_book = books.get_element(p_after)

        if book == next_book:
            next_book.set_times_borrowed(
                book.get_times_borrowed() + next_book.get_times_borrowed()
            )
            books.delete(p)

            # The linked list needs to go to the next position in any case
            if LINKED:
                p = p_after
        elif book.is_older_edition(next_book):
            books.delete(p)

            # The linked list needs to go to the next position in any case
            if LINKED:
                p = p_after

        else:
            # If a book is removed, the next one will be the in the same
            # position as the current one
            p = p_after

    return books


def search_book(books: PositionalList) -> PositionalList:
    """Searches for a book in the list and returns the position of the book.

    Parameters
    ----------
    books : PositionalList
        The list to search in.

    Returns
    -------
    found_books : PositionalList
        The list with the books found.
    """

    found_books = PositionalList()

    while True:
        print(SEARCH_MENU)
        sel = input("> ").lower()
        print()

        # Search by title
        if sel == "1":
            print("Introduce el título del libro:")
            title = input("> ").lower()

            for book in books:
                if book.get_title().lower() == title:
                    found_books.add_last(book)
            break

        # Search by author
        elif sel == "2":
            print("Introduce el autor del libro:")
            author = input("> ").lower()

            for book in books:
                if book.get_author().lower() == author:
                    found_books.add_last(book)
            break

        # Search by year
        elif sel == "3":
            print("Introduce el año del libro:")
            year = int(input("> "))

            for book in books:
                if book.get_year() == year:
                    found_books.add_last(book)
            break

        # Show all books
        elif sel == "4":
            return books

        elif sel == "q":
            break

        else:
            print("Opción inválida")

    return found_books


def main(path):
    """Main function. Reads the file and prints the result."""

    books = None

    while True:
        print(MENU)
        sel = input("> ").lower()
        print()

        # Read the file
        if sel == "1":
            books = get_books(path)
            print(f"{len(books)} libros cargados correctamente.")

        # Show the average of times borrowed
        elif sel == "2":
            if not books:
                print("No hay libros cargados. Usa la opción 1 primero.")
                continue

            media = sum(i.times_borrowed for i in books) / len(books)
            print(f"Media de préstamos: {media}")

        # Remove duplicates
        elif sel == "3":
            if not books:
                print("No hay libros cargados. Usa la opción 1 primero.")
                continue

            original_size = len(books)
            books = deduplicate(books)
            print(f"Libros eliminados: {original_size - len(books)}.")
            print(f"Libros restantes: {len(books)}.")

        # Search books
        elif sel == "4":
            if not books:
                print("No hay libros cargados. Usa la opción 1 primero.")
                continue

            found = search_book(books)

            if found.is_empty():
                print("No se han encontrado libros.")
                continue

            print(f"\nLibros encontrados ({len(found)}):")
            for i in found:
                print(" ", i)

        elif sel == "q":
            sys.exit(0)

        else:
            print("Opción inválida")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("path", help="Path to the file to read")
    parser.add_argument(
        "-l",
        "--linked",
        action="store_true",
        help="Use a linked list instead of an array",
    )
    args = parser.parse_args()

    if args.linked:
        # Override the default list if the user wants to use a linked list
        from linked_positional_list import LinkedPositionalList as PositionalList

        LINKED = True
        print("[Usando linked_positional_list]\n")
    else:
        print("[Usando array_positional_list]\n")

    main(args.path)
