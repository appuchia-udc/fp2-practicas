#! /usr/bin/env python3

# Práctica 3 FP2: Listas posicionales ordenadas
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>


class Book:
    """Book class.

    Attributes
    ----------
    author : str
        The author of the book.
    title : str
        The title of the book.
    year : int
        The year of the book.
    times_borrowed : int
        The number of times the book has been borrowed.

    Methods
    -------
    get_author()
        Returns the author of the book.

    set_author(author: str)
        Sets the author of the book.

    get_title()
        Returns the title of the book.

    set_title(title: str)
        Sets the title of the book.

    get_year()
        Returns the year of the book.

    set_year(year: int)
        Sets the year of the book.

    get_times_borrowed()
        Returns the number of times the book has been borrowed.

    set_times_borrowed(times_borrowed: int)
        Sets the number of times the book has been borrowed.

    is_older_edition(other: Book)
        Returns True if the book is an older edition of the other, False otherwise.
    """

    def __init__(self, author: str, title: str, year: int, times_borrowed: int):
        self.author = author
        self.title = title
        self.year = year
        self.times_borrowed = times_borrowed

    def __str__(self):
        """Returns a string representation of the book.

        Returns
        -------
        str
            The string representation of the book.
        """

        return f"{self.author}: {self.title} ({self.year}). Prestado {self.times_borrowed} veces"

    def __eq__(self, other):
        """Returns True if the books are equal, False otherwise.

        Parameters
        ----------
        other : Book
            The book to compare with.

        Returns
        -------
        bool
            True if the books are equal, False otherwise.
        """

        return (
            self.title == other.title
            and self.author == other.author
            and self.year == other.year
        )

    def __lt__(self, other):
        """Returns True if the book is less than the other, False otherwise.

        Parameters
        ----------
        other : Book
            The book to compare with.

        Returns
        -------
        bool
            True if the book is less than the other, False otherwise.
        """

        if self.author == other.author:
            if self.title == other.title:
                return self.year < other.year
            return self.title < other.title
        return self.author < other.author

    def __gt__(self, other):
        """Returns True if the book is greater than the other, False otherwise.

        Parameters
        ----------
        other : Book
            The book to compare with.

        Returns
        -------
        bool
            True if the book is greater than the other, False otherwise.
        """

        if self.author == other.author:
            if self.title == other.title:
                return self.year > other.year
            return self.title > other.title
        return self.author > other.author

    def __le__(self, other):
        """Returns True if the book is less or equal than the other, False otherwise.

        Parameters
        ----------
        other : Book
            The book to compare with.

        Returns
        -------
        bool
            True if the book is less or equal than the other, False otherwise.
        """

        return self < other or self == other

    def get_author(self):
        """Returns the author of the book.

        Returns
        -------
        str
            The author of the book.
        """

        return self.author

    def set_author(self, author: str):
        """Sets the author of the book.

        Parameters
        ----------
        author : str
            The new author of the book.
        """

        self.author = author

    def get_title(self):
        """Returns the title of the book.

        Returns
        -------
        str
            The title of the book.
        """

        return self.title

    def set_title(self, title: str):
        """Sets the title of the book.

        Parameters
        ----------
        title : str
            The new title of the book.
        """

        self.title = title

    def get_year(self):
        """Returns the year of the book.

        Returns
        -------
        int
            The year of the book.
        """

        return self.year

    def set_year(self, year: int):
        """Sets the year of the book.

        Parameters
        ----------
        year : int
            The new year of the book.
        """

        self.year = year

    def get_times_borrowed(self):
        """Returns the number of times the book has been borrowed.

        Returns
        -------
        int
            The number of times the book has been borrowed.
        """

        return self.times_borrowed

    def set_times_borrowed(self, times_borrowed: int):
        """Sets the number of times the book has been borrowed.

        Parameters
        ----------
        times_borrowed : int
            The new number of times the book has been borrowed.
        """

        self.times_borrowed = times_borrowed

    def is_older_edition(self, other):
        """Returns True if the book is an older edition of the other, False otherwise.

        Parameters
        ----------
        other : Book
            The book to compare with.

        Returns
        -------
        bool
            True if the book is an older edition of the other, False otherwise.
        """

        return (
            self.title == other.title
            and self.author == other.author
            and self.year < other.year
        )
