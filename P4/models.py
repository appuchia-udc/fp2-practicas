#! /usr/bin/env python3

# Práctica 4 FP2: Árboles AVL
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>


class Activity:
    """Activity class

    Attributes
    ----------
    name : str
        Name of the activity
    duration : int
        Duration of the activity in hours
    people : int
        Number of people required to do the activity
    price : float
        Price of the activity per person

    Methods
    -------
    get_name(self) -> str
        Return the name of the activity
    get_duration(self) -> int
        Return the duration of the activity
    get_people(self) -> int
        Return the number of people required to do the activity
    get_price(self) -> float
        Return the price of the activity per person
    set_name(self, name: str) -> None
        Set the name of the activity
    set_duration(self, duration: int) -> None
        Set the duration of the activity
    set_people(self, people: int) -> None
        Set the number of people required to do the activity
    set_price(self, price: float) -> None
        Set the price of the activity per person
    value(self) -> float
        Return the value of the activity
    """

    def __init__(self, name: str, duration: int, people: int, price: float):
        """Constructor

        Parameters
        ----------
        name : str
            Name of the activity
        duration : int
            Duration of the activity in hours
        people : int
            Number of people required to do the activity
        price : float
            Price of the activity per person
        """

        self.name = name
        self.duration = duration
        self.people = people
        self.price = price

    def __eq__(self, other: object) -> bool:
        """Check if two activities are equal

        Parameters
        ----------
        other : object
            Activity to compare with

        Returns
        -------
        bool
            True if the activities are equal, False otherwise
        """

        return (
            self.name == other.get_name()
            and self.duration == other.get_duration()
            and self.people == other.get_people()
            and self.price == other.get_price()
        )

    def __gt__(self, other: object) -> bool:
        """Check if the price of the first activity is greater than the price of the second one

        Parameters
        ----------
        other : object
            Activity to compare with

        Returns
        -------
        bool
            True if the price of the first activity is greater than the price of the second one, False otherwise
        """

        return self.value() > other.value()

    def __lt__(self, other: object) -> bool:
        """Check if the price of the first activity is less than the price of the second one

        Parameters
        ----------
        other : object
            Activity to compare with

        Returns
        -------
        bool
            True if the price of the first activity is less than the price of the second one, False otherwise
        """

        return self.value() < other.value()

    def __ge__(self, other: object) -> bool:
        """Check if the price of the first activity is greater or equal than the price of the second one

        Parameters
        ----------
        other : object
            Activity to compare with

        Returns
        -------
        bool
            True if the price of the first activity is greater or equal than the price of the second one, False otherwise
        """

        return self.value() >= other.value()

    def __le__(self, other: object) -> bool:
        """Check if the price of the first activity is less or equal than the price of the second one

        Parameters
        ----------
        other : object
            Activity to compare with

        Returns
        -------
        bool
            True if the price of the first activity is less or equal than the price of the second one, False otherwise
        """

        return self.value() <= other.value()

    def __str__(self) -> str:
        """Return a string representation of the activity

        Returns
        -------
        str
            String representation of the activity
        """

        return f"{self.name.capitalize()} ({self.duration}h, {self.people}p, {self.price}€)"

    def get_name(self) -> str:
        """Return the name of the activity

        Returns
        -------
        str
            Name of the activity
        """

        return self.name

    def set_name(self, name: str) -> None:
        """Set the name of the activity

        Parameters
        ----------
        name : str
            Name of the activity
        """

        self.name = name

    def get_duration(self) -> int:
        """Return the duration of the activity

        Returns
        -------
        int
            Duration of the activity
        """

        return self.duration

    def set_duration(self, duration: int) -> None:
        """Set the duration of the activity

        Parameters
        ----------
        duration : int
            Duration of the activity
        """

        self.duration = duration

    def get_people(self) -> int:
        """Return the number of people required to do the activity

        Returns
        -------
        int
            Number of people required to do the activity
        """

        return self.people

    def set_people(self, people: int) -> None:
        """Set the number of people required to do the activity

        Parameters
        ----------
        people : int
            Number of people required to do the activity
        """

        self.people = people

    def get_price(self) -> float:
        """Return the price of the activity per person

        Returns
        -------
        float
            Price of the activity per person
        """

        return self.price

    def set_price(self, price: float) -> None:
        """Set the price of the activity per person

        Parameters
        ----------
        price : float
            Price of the activity per person
        """

        self.price = price

    def calc_value(self) -> float:
        """Return the value of the activity

        Returns
        -------
        float
            Value of the activity
        """

        return self.price / self.duration / self.people
