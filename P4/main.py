#! /usr/bin/env python3

# Práctica 4 FP2: Árboles AVL
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

from argparse import ArgumentParser

from avl.avl_tree import AVL
from models import Activity


def print_tree(t: AVL) -> None:
    """Prints the whole tree in a readable format

    Arguments
    ---------
    t : AVL
        AVL tree to print

    Returns
    -------
    None
    """

    def preorder_indent_BST(T, p, d):
        """Print preorder representation of a binary subtree of T rooted at p at depth d.
        To print aTree completely call preorder_indent_BST(aTree, aTree.root(), 0)

        Arguments
        ---------
        T : AVL
            AVL tree
        p : Position
            Position of the root of the subtree
        d : int
            Depth of the subtree

        Returns
        -------
        None
        """
        if p is not None:
            # use depth for indentation
            print(f"{2 * d * ' '}{p.key()} [{p.value()}]")
            preorder_indent_BST(T, T.left(p), d + 1)  # left child depth is d+1
            preorder_indent_BST(T, T.right(p), d + 1)  # right child depth is d+1

    preorder_indent_BST(t, t.root(), 0)


def gen_tree_from_csv(path: str) -> AVL:
    """Generate an AVL tree from a CSV file

    Arguments
    ---------
    path : str
        Path to the CSV file

    Returns
    -------
    tree : AVL
        AVL tree with the data from the CSV file
    """

    with open(path) as f:
        lines = f.readlines()

    tree = AVL()
    for line in lines[1:]:  # Omit CSV header line
        name, duration, people, price = line.strip().split(";")
        tree[name] = Activity(name, int(duration), int(people), float(price))

    return tree


def merge_trees(t1: AVL, t2: AVL) -> AVL:
    """Merge two AVL trees

    Arguments
    ---------
    t1 : AVL
        First tree to merge
    t2 : AVL
        Second tree to merge

    Returns
    -------
    t_merged : AVL
        Merged tree
    """

    t_merged = AVL()
    # Add all elements from t1
    for k in t1.keys():
        t_merged[k] = t1[k]

    # Add elements from t2
    for k in t2.keys():
        # Add new elements
        if k not in t_merged.keys():
            t_merged[k] = t2[k]
        # Add elements with same name and different values
        elif k in t_merged.keys() and t2[k] not in t_merged.values():
            t_merged[k + "_A"] = t1[k]
            t_merged[k + "_B"] = t2[k]
            t_merged.delete(t_merged.find_position(k))

    return t_merged


def intersect_trees(t1: AVL, t2: AVL) -> AVL:
    """Intersect two AVL trees

    Arguments
    ---------
    t1 : AVL
        First tree to intersect
    t2 : AVL
        Second tree to intersect

    Returns
    -------
    t_inters : AVL
        Intersected tree
    """

    t_inters = AVL()

    # Add items from t1 that are in t2 and have a lower value
    for k, v in t1.items():
        if k in t2 and v.calc_value() <= t2[k].calc_value():
            t_inters[k] = v
        elif k in t2 and v.calc_value() > t2[k].calc_value():
            t_inters[k] = t2[k]

    return t_inters


def main(file1: str, file2: str, verbose: bool = False):
    """Main function

    Arguments
    ---------
    file1 : str
        Path to the first file to read
    file2 : str
        Path to the second file to read

    Returns
    -------
    None
    """

    # Generate separate trees
    t1 = gen_tree_from_csv(file1)
    t2 = gen_tree_from_csv(file2)

    if verbose:
        print(f"Árbol 1 ({len(t1)}):")
        print_tree(t1)
        print()
        print(f"Árbol 2 ({len(t2)}):")
        print_tree(t2)
        print()

    # Merge trees ("Suma de actividades")
    t_merged = merge_trees(t1, t2)
    print(f"Suma de actividades. ({len(t_merged)}):")
    print_tree(t_merged)
    print()

    # Intersection of trees ("Oferta mínima común")
    t_inters = intersect_trees(t1, t2)
    print(f"Oferta mínima común. ({len(t_inters)}):")
    print_tree(t_inters)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("file1", help="Path to the first file to read")
    parser.add_argument("file2", help="Path to the second file to read")
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose mode")
    args = parser.parse_args()

    main(args.file1, args.file2, args.verbose)
