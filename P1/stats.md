# Estadísticas finales

Resultados de la simulación de 30 combates.

> Este archivo usa Markdown.
> El contenido podrá ser visualizado copiándolo en un editor web como
> [StackEdit](https://stackedit.io/app) en caso de no disponer de uno en local.

## Número de victorias por Avatar

Nombre | Victorias
:--- | ---:
Priest18 | 3
Warrior26 | 2
Mage14 | 2
Mage27 | 2
Mage32 | 2
Warrior1 | 1
Warrior5 | 1
Warrior19 | 1
Warrior20 | 1
Warrior22 | 1
Warrior23 | 1
Warrior25 | 1
Warrior29 | 1
Warrior30 | 1
Warrior32 | 1
Mage0 | 1
Mage15 | 1
Mage20 | 1
Mage26 | 1
Mage29 | 1
Priest5 | 1
Priest8 | 1
Priest9 | 1
Priest17 | 1

El resto de avatares no han ganado ninguna batalla.

## Daño medio y desviación típica por Avatar

Nombre | Daño medio | Desviación típica
:--- | :---: | ---:
Mage22 | 114.00 | 4.70
Mage14 | 112.36 | 3.91
Mage2 | 110.77 | 4.01
Mage24 | 110.00 | 3.96
Mage11 | 109.71 | 4.22
Mage21 | 109.68 | 4.80
Mage17 | 108.39 | 4.81
Mage30 | 108.00 | 3.91
Mage8 | 106.86 | 5.31
Mage5 | 105.25 | 5.03
Mage4 | 104.61 | 3.61
Mage25 | 104.60 | 4.50
Mage28 | 101.95 | 4.04
Mage18 | 100.59 | 4.25
Mage1 | 100.57 | 3.58
Mage29 | 99.27 | 4.55
Mage9 | 95.85 | 3.93
Mage26 | 93.74 | 3.68
Mage15 | 93.72 | 4.27
Mage12 | 93.26 | 4.67
Mage13 | 92.85 | 3.99
Mage16 | 92.79 | 4.37
Mage27 | 90.24 | 3.21
Mage6 | 89.42 | 3.79
Mage31 | 88.89 | 5.26
Mage3 | 88.71 | 3.86
Mage23 | 88.65 | 4.47
Mage7 | 87.04 | 4.98
Mage0 | 86.92 | 4.53
Mage10 | 85.69 | 5.44
Mage20 | 83.44 | 5.02
Mage32 | 82.20 | 4.94
Mage19 | 81.80 | 3.78
Warrior22 | 43.05 | 6.87
Warrior29 | 40.16 | 11.60
Warrior13 | 39.29 | 6.86
Warrior8 | 38.13 | 9.69
Warrior24 | 37.67 | 7.54
Warrior1 | 37.17 | 8.54
Warrior28 | 36.93 | 8.59
Warrior9 | 35.31 | 12.29
Warrior32 | 35.14 | 10.00
Warrior30 | 34.54 | 12.55
Warrior16 | 34.50 | 10.39
Warrior6 | 34.24 | 17.54
Warrior25 | 32.58 | 8.60
Warrior18 | 29.63 | 10.66
Warrior3 | 28.56 | 5.60
Warrior31 | 28.41 | 11.59
Warrior4 | 28.20 | 14.23
Warrior26 | 28.18 | 9.65
Warrior10 | 27.27 | 6.78
Warrior20 | 27.17 | 12.12
Warrior2 | 25.18 | 7.20
Warrior0 | 25.11 | 11.54
Warrior5 | 24.55 | 6.89
Warrior19 | 24.26 | 9.55
Warrior27 | 24.05 | 7.73
Warrior23 | 23.78 | 14.47
Warrior15 | 23.61 | 10.40
Warrior7 | 23.00 | 8.53
Warrior11 | 22.30 | 6.31
Priest14 | 21.00 | 5.22
Priest29 | 20.96 | 4.32
Warrior12 | 20.47 | 14.87
Warrior14 | 19.90 | 15.07
Priest6 | 19.72 | 4.91
Priest8 | 19.54 | 4.56
Priest17 | 19.41 | 3.85
Priest26 | 18.86 | 3.45
Priest13 | 18.60 | 5.03
Priest1 | 18.46 | 3.99
Priest24 | 17.76 | 4.43
Priest31 | 17.74 | 4.02
Priest27 | 17.27 | 4.29
Priest20 | 16.72 | 4.64
Priest22 | 16.50 | 3.20
Priest10 | 16.00 | 3.33
Priest25 | 15.48 | 3.89
Priest18 | 15.17 | 3.33
Priest12 | 15.11 | 3.84
Priest5 | 15.10 | 4.64
Warrior17 | 14.97 | 15.89
Priest23 | 14.67 | 4.75
Priest7 | 14.56 | 4.08
Priest2 | 14.00 | 3.71
Priest32 | 13.63 | 3.72
Priest19 | 13.18 | 4.12
Priest21 | 13.14 | 4.58
Priest30 | 13.07 | 3.63
Priest16 | 12.78 | 3.11
Priest11 | 12.59 | 2.88
Priest9 | 12.14 | 4.59
Priest4 | 11.96 | 4.83
Priest28 | 11.84 | 3.96
Priest0 | 11.07 | 4.35
Priest15 | 10.80 | 4.73
Priest3 | 10.43 | 3.10
Warrior21 | 8.30 | 17.57

## Curación media y desviación típica por Priest

Nombre | Curación media | Desviación típica
:--- | :---: | ---:
Priest28 | 11.00 | 0.00
Priest13 | 9.33 | 6.60
Priest29 | 8.75 | 7.40
Priest11 | 8.25 | 4.76
Priest5 | 7.33 | 5.19
Priest7 | 7.20 | 5.88
Priest17 | 6.93 | 6.49
Priest2 | 6.88 | 5.33
Priest25 | 6.55 | 5.98
Priest1 | 6.50 | 6.50
Priest6 | 5.57 | 6.43
Priest9 | 5.56 | 4.97
Priest19 | 5.08 | 5.48
Priest21 | 5.08 | 5.48
Priest18 | 4.70 | 5.86
Priest10 | 4.50 | 5.81
Priest31 | 4.33 | 6.13
Priest8 | 4.00 | 6.32
Priest12 | 4.00 | 5.66
Priest24 | 3.75 | 5.56
Priest16 | 3.14 | 4.97
Priest26 | 2.80 | 5.60
Priest3 | 2.50 | 4.33
Priest32 | 2.50 | 4.33
Priest20 | 2.40 | 4.80
Priest14 | 2.31 | 5.41
Priest22 | 1.86 | 4.55

El resto de avatares no se han curado.

---

## Número de victorias por clase

Clase | Victorias
:--- | ---:
Warrior | 12
Mage | 11
Priest | 7

## Daño medio y desviación típica por clase

Clase | Daño medio | Desviación típica
--- | :---: | ---
Warrior | 29.27 | 1.217
Mage | 97.408 | 1.603
Priest | 15.481 | 0.555
