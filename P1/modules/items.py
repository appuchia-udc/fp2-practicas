#! /usr/bin/env python3

# Práctica 1 FP2: Simulaciones de combates
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>


class Item:
    """Abstract class for items.

    Does not have any attributes or methods.
    """

    def __init__(self):
        """Constructor for Item.

        Does nothing.
        """
        pass


class Weapon(Item):
    """Defines a weapon.

    Class that defines a weapon.
    It has a power attribute, which is used to calculate total damage dealt.
    It can be used by an Avatar.


    Attributes
    ----------
    `power` : `int`
        Power of the weapon.

    Methods
    -------
    `get_power()`:
        Returns the power of the weapon.
    `set_power(power: int)`:
        Sets the power of the weapon.
    """

    def __init__(self, power: int):
        """Constructor for Weapon.

        Parameters
        ----------
        `power` : `int`
            The power of the weapon.
        """
        self.power = power

    def __repr__(self) -> str:
        """Returns a string representation of the weapon.

        Returns
        -------
        A string representation of the weapon.
        """
        return f"Power: {self.power}"

    def __str__(self) -> str:
        """Returns a string representation of the item. (Alias for __repr__)

        Returns
        -------
        A string representation of the item.
        """
        return self.__repr__()

    def get_power(self) -> int:
        """Returns the power of the weapon.

        Returns
        -------
        Power of the weapon as an int.
        """
        return self.power

    def set_power(self, power: int) -> None:
        """Sets the power of the weapon.

        Parameters
        ----------
        `power` : `int`
            The power of the weapon.

        Returns
        -------
        `None`.
        """
        self.power = power


class Covering(Item):
    """Defines a covering item.

    Used to define a covering item such as armor or a shield.
    It has a protection attribute, which is used to calculate total damage received.
    It can be held by an Avatar.


    Attributes
    ----------
    `protection` : `int`
        Preotection of the item.

    Methods
    -------
    `get_protection()`:
        Returns the protection of the item.
    `set_protection(protection: int)`:
        Sets the protection of the item.
    """

    def __init__(self, protection: int):
        """Constructor for Covering.

        Parameters
        ----------
        `protection` : `int`
            The protection of the item.
        """
        self.protection = protection

    def __repr__(self) -> str:
        """Returns a string representation of the item.

        Returns
        -------
        A string representation of the item.
        """
        return f"Protection: {self.protection}"

    def __str__(self) -> str:
        """Returns a string representation of the item. (Alias for __repr__)

        Returns
        -------
        A string representation of the item.
        """
        return self.__repr__()

    def get_protection(self) -> int:
        """Returns the protection of the item.

        Returns
        -------
        Protection of the item as an int.
        """
        return self.protection

    def set_protection(self, protection: int) -> None:
        """Sets the protection of the item.

        Parameters
        ----------
        `protection` : `int`
            The protection of the item.

        Returns
        -------
        `None`.
        """
        self.protection = protection


class Sword(Weapon):
    """A melee type of weapon.

    Defines a sword, a melee type of weapon, that can be held by `Melee` avatars.
    """

    def __init__(self, power: int):
        """Constructor for Sword.

        Parameters
        ----------
        `power` : `int`
            The power of the weapon.
        """
        super().__init__(power)


class Wand(Weapon):
    """A ranged type of weapon.

    Defines a Wand, a melee type of weapon, that can be held by `Caster` avatars.
    """

    def __init__(self, power: int):
        """Constructor for Wand.

        Parameters
        ----------
        `power` : `int`
            The power of the weapon.
        """
        super().__init__(power)


class Shield(Covering):
    """A melee type of covering item.

    Defines a shield, a melee type of cover, that can be held by `Melee` avatars.
    """

    def __init__(self, protection: int):
        """Constructor for Shield.

        Parameters
        ----------
        `protection` : `int`
            The protection of the item.
        """
        super().__init__(protection)


class Armor(Covering):
    """Covering item that grants protection.

    Defines armor. It is a covering item that can be held by any avatar and reduces damage received.
    """

    def __init__(self, protection: int):
        """Constructor for Armor.

        Parameters
        ----------
        `protection` : `int`
            The protection of the item.
        """
        super().__init__(protection)
