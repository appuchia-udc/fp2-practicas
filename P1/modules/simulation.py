#! /usr/bin/env python3

# Práctica 1 FP2: Simulaciones de combates
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

from modules.avatars import Avatar, Warrior, Mage, Priest
from modules.items import Item, Sword, Wand, Armor, Shield
from modules.statistics import Stats
from settings import MAX_ITEM_VALUE, get_verboisty
import random


def simulate(alive: list[Avatar], stats: dict[str, Stats]) -> Avatar:
    """Runs a simulation of a combat between two Avatars.

    Parameters
    ----------
    `alive` : `list[Avatar]`
        List of alive Avatars.

    Returns
    -------
    The `Avatar` that won. [Subject to change]
    """
    while len(alive) > 1:
        attacker = alive.pop(random.randint(0, len(alive) - 1))
        defender = alive.pop(random.randint(0, len(alive) - 1))

        # Re-add alive avatars (dead ones are kept out of the list)
        alive.extend(_fight(attacker, defender, stats))

        # Spawn items
        if spawned_items := _spawn_items():
            for item in spawned_items:
                _assign_item(attacker, item)

    # Add win to stats
    stats[alive[0].get_name()].add_win()

    return alive[0]


def _fight(attacker: Avatar, defender: Avatar, stats: dict[str, Stats]) -> list[Avatar]:
    """Runs a single fight between two Avatars.

    Parameters
    ----------
    `attacker` : `Avatar`
        The Avatar that attacks.
    `defender` : `Avatar`
        The Avatar that defends.

    Returns
    -------
    Avatars still alive after the fight.
    """
    if isinstance(attacker, Priest):
        # Heal 25% of times (only if randint returns 0)
        if not random.randint(0, 3):
            healed = attacker.heal()

            # Add stats
            stats[attacker.get_name()].add_heal(healed)

            return [attacker, defender]

    damage = attacker.attack() - defender.defend()
    new_health = defender.get_life() - damage

    # Add stats
    stats[attacker.get_name()].add_damage(damage)

    if new_health > 0:
        defender.set_life(new_health)
        return [attacker, defender]
    else:
        if get_verboisty() > 2:
            print(f"[!] - {defender!s:<35} ha muerto a manos de:  {attacker}")
        return [attacker]


def _spawn_items() -> list[Item] | None:
    """Spawns weapon and covering items.

    Returns
    -------
    `list[Item]` with the spawned items.
    """
    items = list()

    # Generate possible items
    weapons = Sword(random.randint(1, MAX_ITEM_VALUE)), Wand(
        random.randint(1, MAX_ITEM_VALUE)
    )
    covering = Armor(random.randint(1, MAX_ITEM_VALUE)), Shield(
        random.randint(1, MAX_ITEM_VALUE)
    )

    # Generate weapon
    if random.randint(0, 1):
        items.append(random.choice(weapons))
    if random.randint(0, 1):
        items.append(random.choice(covering))

    # If no items were generated, return None
    if not items:
        return None
    return items


def _assign_item(avatar: Avatar, item: Item) -> None:
    """Assigns an item to an Avatar if it's betterthan the current
    one and it can be equipped.

    Parameters
    ----------
    `avatar` : `Avatar`
        The Avatar to assign the item to.
    `item` : `Item`
        The Item to assign to the Avatar.

    Returns
    -------
    `None`.
    """

    # `type: ignore` is used to ignore the `type error` raised by mypy

    equipped = False

    # Add weapons
    if isinstance(item, Sword) and isinstance(avatar, Warrior):
        if item.power > (avatar.get_weapon().power if avatar.get_weapon() else 0):  # type: ignore
            avatar.set_weapon(item)
            equipped = True
    elif isinstance(item, Wand) and isinstance(avatar, Mage):
        if item.power > (avatar.get_weapon().power if avatar.get_weapon() else 0):  # type: ignore
            avatar.set_weapon(item)
            equipped = True

    # Add armor
    elif isinstance(item, Armor):
        if avatar.get_armor():
            if item.protection > avatar.armor.protection:  # type: ignore
                avatar.set_armor(item)
                equipped = True
        else:
            avatar.set_armor(item)
            equipped = True
    # Add shield
    elif isinstance(item, Shield) and isinstance(avatar, Warrior):
        if avatar.get_shield():
            if item.protection > avatar.shield.protection:  # type: ignore
                avatar.set_shield(item)
                equipped = True
        else:
            avatar.set_shield(item)
            equipped = True

    if get_verboisty() >= 1:
        if equipped:
            print(f"[·] - {avatar} ha encontrado {item} y se lo ha equipado")
