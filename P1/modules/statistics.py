#! /usr/bin/env python3

# Práctica 1 FP2: Simulaciones de combates
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

from settings import get_simulations


class Stats:
    """Used to store statistics about the simulations.

    Attributes
    ----------
    `class` : `str`
        The class of the Avatar. (Warrior or Mage)
    `name` : `str`
        The name of the Avatar.
    `wins` : `int`
        Number of wins.
    `damages` : `list[int]`
        Total damage dealt.
    `fights` : `int`
        Number of fights.
    `healed` : `int`
        Total amount of health healed.

    Methods
    -------
    `add_win()`:
        Adds a win to the stats.
    `add_damage(damage: int)`:
        Adds damage to the stats.
    `add_fight()`:
        Adds a fight to the stats.
    `avg_damage()`:
        Calculates the average damage dealt.
    `damage_stddev()`:
        Calculates the standard deviation of the damage dealt.
    """

    def __init__(self, pj_class: str, name: str) -> None:
        """Constructor for Stats.

        Parameters
        ----------
        `class` : `str`
            The class of the Avatar. (Warrior or Mage)
        `name` : `str`
            The name of the Avatar.

        Returns
        -------
        `None`.
        """
        self.pj_class = pj_class
        self.name = name
        self.wins: int = 0
        self.damages: list[int] = list()
        self.fights: int = 0
        self.heals: list[int] = list()

    def __repr__(self) -> str:
        """Returns a string representation of the stats.

        Returns
        -------
        A string representation of the stats.
        """
        return f"{self.name}: {self.wins} wins, {sum(self.damages)} damage dealt, {self.fights} fights"

    def __str__(self) -> str:
        """Returns a string representation of the stats. (Alias for __repr__)

        Returns
        -------
        A string representation of the stats.
        """
        return self.__repr__()

    def add_win(self) -> None:
        """Adds a win to the stats.

        Returns
        -------
        `None`.
        """
        self.wins += 1

    def add_damage(self, damage: int) -> None:
        """Adds damage to the stats. It also increases the fight counter.

        Parameters
        ----------
        `damage` : `int`
            The damage to add.

        Returns
        -------
        `None`.
        """
        self.damages.append(damage)
        self.fights += 1

    def add_heal(self, heal: int) -> None:
        """Adds heal to the stats. It also increases the fight counter.

        Parameters
        ----------
        `heal` : `int`
            The heal to add.

        Returns
        -------
        `None`.
        """
        self.heals.append(heal)

    def avg_damage(self) -> float:
        """Calculates the average damage dealt.

        Returns
        -------
        The average damage dealt.
        """
        return sum(self.damages) / len(self.damages)

    def damage_stddev(self) -> float:
        """Calculates the standard deviation of the damage dealt.

        Returns
        -------
        The standard deviation of the damage dealt.
        """
        average = self.avg_damage()
        return (
            sum((x - average) ** 2 for x in self.damages) / len(self.damages)
        ) ** 0.5

    def avg_heal(self) -> float:
        """Calculates the average heal.

        Returns
        -------
        The average heal.
        """
        if not sum(self.heals):
            return 0

        return sum(self.heals) / len(self.heals)

    def heal_stddev(self) -> float:
        """Calculates the standard deviation of the heal.

        Returns
        -------
        The standard deviation of the heal.
        """
        if not sum(self.heals):
            return 0

        average = self.avg_heal()
        return (sum((x - average) ** 2 for x in self.heals) / len(self.heals)) ** 0.5


# Join all the stats
def calc_stats(
    stats: list[Stats],
) -> tuple[
    list[Stats],
    list[Stats],
    list[Stats],
    tuple[int, int, int],
    tuple[tuple[float, float, float], tuple[float, float, float]],
]:
    """Calculates all the stats required.

    Parameters
    ----------
    `stats` : `list[Stats]`
        The stats to calculate the average damage dealt and its standard deviation.

    Returns
    -------
    Sorted in descending order:
        - Stats sorted by win count.
        - Stats sorted by average damage dealt.
        - Tuple of win count for each class (Warrior, Mage).
        - Tuple of average damage dealt and standard deviation for each class.
    """

    return *calc_avatar_stats(stats), *calc_class_stats(stats)


# Calculate the avatar stats
def calc_avatar_stats(
    stats: list[Stats],
) -> tuple[list[Stats], list[Stats], list[Stats]]:
    """Calculates all the avatar stats required.

    Parameters
    ----------
    `stats` : `list[Stats]`
        The stats to calculate the average damage dealt and its standard deviation.

    Returns
    -------
    Tuple of 2 lists of:
        - Stats sorted in descending order by win count.
        - Stats sorted in descending order by average damage dealt.
    """

    # Sorted stats by wins
    avatar_win_count = sorted(stats, key=lambda x: x.wins, reverse=True)

    # Avatar average damage dealt and standard deviation
    avatar_avgdmg_stddev = sorted(stats, key=lambda x: x.avg_damage(), reverse=True)

    # Avatar average heal and standard deviation
    avatar_heal_avgdmg_stddev = sorted(stats, key=lambda x: x.avg_heal(), reverse=True)

    return avatar_win_count, avatar_avgdmg_stddev, avatar_heal_avgdmg_stddev


# Calculate the class stats
def calc_class_stats(
    stats: list[Stats],
) -> tuple[
    tuple[int, int, int], tuple[tuple[float, float, float], tuple[float, float, float]]
]:
    """Calculates all the class stats required.

    Parameters
    ----------
    `stats` : `list[Stats]`
        The stats to calculate the average damage dealt and its standard deviation.

    Returns
    -------
    Tuple of 2 tuples of:
        - Win count for each class (Warrior, Mage).
        - Average damage dealt and standard deviation for each class.
    """

    # Calculate win count for each class
    warrior_win_count = sum(stat.wins for stat in stats if stat.pj_class == "Warrior")
    mage_win_count = sum(stat.wins for stat in stats if stat.pj_class == "Mage")
    priest_win_count = sum(stat.wins for stat in stats if stat.pj_class == "Priest")

    # Calculate average damage dealt and standard deviation for each class
    warriors = [stat for stat in stats if stat.pj_class == "Warrior"]
    mages = [stat for stat in stats if stat.pj_class == "Mage"]
    priests = [stat for stat in stats if stat.pj_class == "Priest"]

    warriors_total_damage = sum(sum(stat.damages) for stat in warriors)
    warriors_total_attacks = sum(len(stat.damages) for stat in warriors)
    warriors_avgdmg = round(warriors_total_damage / warriors_total_attacks, 3)

    mages_total_damage = sum(sum(stat.damages) for stat in mages)
    mages_total_attacks = sum(len(stat.damages) for stat in mages)
    mages_avgdmg = round(mages_total_damage / mages_total_attacks, 3)

    priests_total_damage = sum(sum(stat.damages) for stat in priests)
    priests_total_attacks = sum(len(stat.damages) for stat in priests)
    priests_avgdmg = round(priests_total_damage / priests_total_attacks, 3)

    warriors_stddev = round(
        (
            sum(
                (sum(x.damages) / len(x.damages) - warriors_avgdmg) ** 2
                for x in warriors
            )
            / warriors_total_attacks
        )
        ** 0.5,
        3,
    )
    mages_stddev = round(
        (
            sum((sum(x.damages) / len(x.damages) - mages_avgdmg) ** 2 for x in mages)
            / mages_total_attacks
        )
        ** 0.5,
        3,
    )
    priests_stddev = round(
        (
            sum(
                (sum(x.damages) / len(x.damages) - priests_avgdmg) ** 2 for x in priests
            )
            / priests_total_attacks
        )
        ** 0.5,
        3,
    )

    return (warrior_win_count, mage_win_count, priest_win_count), (
        (warriors_avgdmg, mages_avgdmg, priests_avgdmg),
        (warriors_stddev, mages_stddev, priests_stddev),
    )


# Display the stats
def display_stats(
    avatar_win_count: list[Stats],
    avatar_avgdmg_stddev: list[Stats],
    avatar_heal_avgdmg_stddev: list[Stats],
    class_win_count: tuple[int, int, int],
    class_avgdmg_stddev: tuple[tuple[float, float, float], tuple[float, float, float]],
) -> None:
    """Displays all the stats required.

    Parameters
    ----------
    `avatar_win_count` : `list[Stats]`
        The stats sorted in descending order by win count.
    `avatar_avgdmg_stddev` : `list[Stats]`
        The stats sorted in descending order by average damage dealt.
    `class_win_count` : `tuple[int, int]`
        The win count for each class (Warrior, Mage).
    `class_avgdmg_stddev` : `tuple[tuple[float, float], tuple[float, float]]`
        The average damage dealt and standard deviation for each class.

    Returns
    -------
    `None`.
    """
    warrior_win_count, mage_win_count, priest_win_count = class_win_count
    warrior_avgdmg, mage_avgdmg, priest_avgdmg = class_avgdmg_stddev[0]
    warrior_stddev, mage_stddev, priest_stddev = class_avgdmg_stddev[1]

    avatar_win_count_str = (
        "Nombre | Victorias\n"
        + ":--- | ---:\n"
        + "\n".join(
            [
                f"{stat.name} | {stat.wins}"
                for stat in filter(lambda x: x.wins, avatar_win_count)
            ]
        )
        + "\n\nEl resto de avatares no han ganado ninguna batalla."
    )

    avatar_avgdmg_stddev_str = (
        "Nombre | Daño medio | Desviación típica\n"
        + ":--- | :---: | ---:\n"
        + "\n".join(
            [
                f"{stat.name} | {stat.avg_damage():.2f} | {stat.damage_stddev():.2f}"
                for stat in avatar_avgdmg_stddev
            ]
        )
    )

    avatar_heal_avgdmg_stddev_str = (
        "Nombre | Curación media | Desviación típica\n"
        + ":--- | :---: | ---:\n"
        + "\n".join(
            [
                f"{stat.name} | {stat.avg_heal():.2f} | {stat.heal_stddev():.2f}"
                for stat in filter(lambda x: sum(x.heals), avatar_heal_avgdmg_stddev)
            ]
        )
        + "\n\nEl resto de avatares no se han curado."
    )

    with open("stats.md", "w") as f:
        f.write(
            f"""# Estadísticas finales

Resultados de la simulación de {get_simulations()} combates.

> Este archivo usa Markdown.
> El contenido podrá ser visualizado copiándolo en un editor web como
> [StackEdit](https://stackedit.io/app) en caso de no disponer de uno en local.

## Número de victorias por Avatar

{avatar_win_count_str}

## Daño medio y desviación típica por Avatar

{avatar_avgdmg_stddev_str}

## Curación media y desviación típica por Priest

{avatar_heal_avgdmg_stddev_str}

---

## Número de victorias por clase

Clase | Victorias
:--- | ---:
Warrior | {warrior_win_count}
Mage | {mage_win_count}
Priest | {priest_win_count}

## Daño medio y desviación típica por clase

Clase | Daño medio | Desviación típica
--- | :---: | ---
Warrior | {warrior_avgdmg} | {warrior_stddev}
Mage | {mage_avgdmg} | {mage_stddev}
Priest | {priest_avgdmg} | {priest_stddev}
"""
        )

        short_avatar_win_count_str = (
            "Nombre    | Victorias\n--------- | ---------\n"
            + "\n".join(
                [f"{stat.name:<9} | {stat.wins}" for stat in avatar_win_count[:10]]
            )
        )

        short_avatar_avgdmg_stddev_str = (
            "Nombre    | Daño medio | Desviación típica\n--------- | ---------- | -----------------\n"
            + "\n".join(
                [
                    f"{stat.name:<9} | {round(stat.avg_damage(), 2):<10} | {stat.damage_stddev():.2f}"
                    for stat in avatar_avgdmg_stddev[:10]
                ]
            )
        )

        short_avatar_heal_avgdmg_stddev_str = (
            "Nombre   | Curación media | Desviación típica\n-------- | -------------- | -----------------\n"
            + "\n".join(
                [
                    f"{stat.name:<8} | {round(stat.avg_heal(), 2):<14} | {stat.heal_stddev():.2f}"
                    for stat in avatar_heal_avgdmg_stddev[:10]
                ]
            )
        )

        print(
            f"""
# #################### #
# Estadísticas finales #
# #################### #

Resultados de la simulación de {get_simulations()} combates.

# Número de victorias por Avatar (10 primeros)
{short_avatar_win_count_str}

# Daño medio y desviación típica por Avatar (10 primeros)
{short_avatar_avgdmg_stddev_str}

# Curación media y desviación típica por Priest (10 primeros)
{short_avatar_heal_avgdmg_stddev_str}

# Número de victorias por clase
Clase   | Victorias
------- | ---------
Warrior | {warrior_win_count}
Mage    | {mage_win_count}
Priest  | {priest_win_count}

# Daño medio y desviación típica por clase
Clase   | Daño medio | Desviación típica
------- | ---------- | -----------------
Warrior | {warrior_avgdmg:<10} | {warrior_stddev}
Mage    | {mage_avgdmg:<10} | {mage_stddev}
Priest  | {priest_avgdmg:<10} | {priest_stddev}

La información completa se ha guardado en el archivo ./stats.md
"""
        )
