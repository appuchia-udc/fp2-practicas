#! /usr/bin/env python3

# Práctica 1 FP2: Simulaciones de combates
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

import random
from modules.items import Weapon, Sword, Wand, Armor, Shield


class Avatar:
    """Abstract class for defining avatars.

    Class that defines an avatar. It has a name, life, strength and defense
    attributes. It can be equipped with a weapon and/or an armor.

    Attributes
    ----------
    `name` : `str`
        The name of the avatar.
    `life` : `int`
        The life of the avatar.
    `strength` : `int`
        The strength of the avatar.
    `defense` : `int`
        The defense of the avatar.
    `weapon` : `Weapon | None`
        The weapon of the avatar. Initially `None`.
    `armor` : `Armor | None`
        The armor of the avatar. Initially `None`.


    Methods
    -------
    `get_name()`:
        Returns the name of the avatar.
    `set_name(name: str)`:
        Sets the name of the avatar.
    `get_life()`:
        Returns the life of the avatar.
    `set_life(life: int)`:
        Sets the life of the avatar.
    `get_strength()`:
        Returns the strength of the avatar.
    `set_strength(strength: int)`:
        Sets the strength of the avatar.
    `get_defense()`:
        Returns the defense of the avatar.
    `set_defense(defense: int)`:
        Sets the defense of the avatar.
    `get_weapon()`:
        Returns the weapon of the avatar.
    `set_weapon(weapon: Weapon)`:
        Sets the weapon of the avatar.
    `get_armor()`:
        Returns the armor of the avatar.
    `set_armor(armor: Armor)`:
        Sets the armor of the avatar.
    `attack()`:
        Returns the damage dealt by the avatar.
    `defend(damage: int)`:
        Returns the damage blocked by the avatar.
    """

    def __init__(self, name: str, life: int, strength: int, defense: int):
        """Initializes the avatar.

        Parameters
        ----------
        `name` : `str`
            The name of the avatar.
        `life` : `int`
            The life of the avatar.
        `strength` : `int`
            The strength of the avatar.
        `defense` : `int`
            The defense of the avatar.
        """

        self.name = name
        self.life = life
        self.strength = strength
        self.defense = defense
        self.weapon: Weapon | None = None
        self.armor: Armor | None = None

    def __repr__(self) -> str:
        """Returns a string representation of the avatar.

        Returns
        -------
        A string representation of the avatar.
        """
        return (
            f"{self.name} ({self.life} HP) [{self.strength} ATK] [{self.defense} DEF]"
        )

    def __str__(self) -> str:
        """Returns a string representation of the avatar. (Alias for __repr__)

        Returns
        -------
        A string representation of the avatar.
        """
        return self.__repr__()

    def get_name(self) -> str:
        """Returns the name of the avatar.

        Returns
        -------
        Name of the avatar as a `str`.
        """
        return self.name

    def set_name(self, name: str) -> None:
        """Sets the name of the avatar.

        Parameters
        ----------
        `name` : `str`
            The name of the avatar.

        Returns
        -------
        `None`.
        """
        self.name = name

    def get_life(self) -> int:
        """Returns the life of the avatar.

        Returns
        -------
        Life of the avatar as an `int`.
        """
        return self.life

    def set_life(self, life: int) -> None:
        """Sets the life of the avatar.

        Parameters
        ----------
        `life` : `int`
            The life of the avatar.

        Returns
        -------
        `None`.
        """
        self.life = life

    def get_strength(self) -> int:
        """Returns the strength of the avatar.

        Returns
        -------
        Strength of the avatar as an `int`.
        """
        return self.strength

    def set_strength(self, strength: int) -> None:
        """Sets the strength of the avatar.

        Parameters
        ----------
        `strength` : `int`
            The strength of the avatar.

        Returns
        -------
        `None`.
        """
        self.strength = strength

    def get_defense(self) -> int:
        """Returns the defense of the avatar.

        Returns
        -------
        Defense of the avatar as an `int`.
        """
        return self.defense

    def set_defense(self, defense: int) -> None:
        """Sets the defense of the avatar.

        Parameters
        ----------
        `defense` : `int`
            The defense of the avatar.

        Returns
        -------
        `None`.
        """
        self.defense = defense

    def get_weapon(self) -> Weapon | None:
        """Returns the weapon of the avatar.

        Returns
        -------
        Weapon of the avatar as a `Weapon`.
        """
        return self.weapon

    def set_weapon(self, weapon: Weapon) -> None:
        """Sets the weapon of the avatar.

        Parameters
        ----------
        `weapon` : `Weapon`
            The weapon of the avatar.

        Returns
        -------
        `None`.
        """
        self.weapon = weapon

    def get_armor(self) -> Armor | None:
        """Returns the armor of the avatar.

        Returns
        -------
        Armor of the avatar as an `Armor`.
        """
        return self.armor

    def set_armor(self, armor: Armor) -> None:
        """Sets the armor of the avatar.

        Parameters
        ----------
        `armor` : `Armor`
            The armor of the avatar.

        Returns
        -------
        `None`.
        """
        self.armor = armor

    def attack(self) -> int:
        """Returns the damage dealt by the avatar.

        Returns
        -------
        Damage dealt by the avatar as an `int`.
        """
        return self.strength

    def defend(self) -> int:
        """Returns the damage blocked by the avatar.

        Returns
        -------
        Damage blocked by the avatar as an `int`.
        """
        return self.defense


class Melee(Avatar):
    """Class for defining melee avatars.

    Class that defines a melee avatar. It has a name, life, strength and
    defense. It can be equipped with a weapon (a `Sword`) and/or an armor.

    Attributes
    ----------
    `name` : `str`
        The name of the avatar.
    `life` : `int`
        The life of the avatar.
    `strength` : `int`
        The strength of the avatar.
    `defense` : `int`
        The defense of the avatar.
    `weapon` : `Weapon | None`
        The weapon of the avatar. Initially `None`.
    `armor` : `Armor | None`
        The armor of the avatar. Initially `None`.
    `shield` : `Shield | None`
        The shield of the avatar. Initially `None`.

    Methods
    -------
    `get_shield()`:
        Returns the shield of the avatar.
    `set_shield(shield: Shield)`:
        Sets the shield of the avatar.
    `set_weapon(weapon: Weapon)`:
        Sets the weapon of the avatar.
    """

    def __init__(self, name: str, life: int, strength: int, defense: int) -> None:
        """Initializes the melee avatar.

        Parameters
        ----------
        `name` : `str`
            The name of the avatar.
        `life` : `int`
            The life of the avatar.
        `strength` : `int`
            The strength of the avatar.
        `defense` : `int`
            The defense of the avatar.
        """
        super().__init__(name, life, strength, defense)

        self.shield: Shield | None = None

    shield: Shield | None = None

    def get_shield(self) -> Shield | None:
        """Returns the shield of the avatar.

        Returns
        -------
        Shield of the avatar as a `Shield`.
        """
        return self.shield

    def set_shield(self, shield: Shield) -> None:
        """Sets the shield of the avatar.

        Parameters
        ----------
        `shield` : `Shield`
            The shield of the avatar.

        Returns
        -------
        `None`.
        """
        self.shield = shield

    def set_weapon(self, weapon: Sword) -> None:
        """Sets the weapon of the avatar.

        Parameters
        ----------
        `weapon` : `Sword`
            The weapon of the avatar.

        Returns
        -------
        `None`.
        """
        assert isinstance(weapon, Sword), "The given weapon must be a sword."
        return super().set_weapon(weapon)


class Caster(Avatar):
    """Class for defining caster avatars.

    Class that defines a caster avatar. It has a name, life, strength and
    defense. It can be equipped with a weapon (a `Wand`) and/or an armor.

    Attributes
    ----------
    `name` : `str`
        The name of the avatar.
    `life` : `int`
        The life of the avatar.
    `strength` : `int`
        The strength of the avatar.
    `defense` : `int`
        The defense of the avatar.

    """

    def __init__(self, name: str, life: int, strength: int, defense: int) -> None:
        """Initializes the caster avatar.

        Parameters
        ----------
        `name` : `str`
            The name of the avatar.
        `life` : `int`
            The life of the avatar.
        `strength` : `int`
            The strength of the avatar.
        `defense` : `int`
            The defense of the avatar.
        """
        super().__init__(name, life, strength, defense)

        self.mana: int = 0

    def get_mana(self) -> int:
        """Returns the mana of the avatar.

        Returns
        -------
        Mana of the avatar as an `int`.
        """
        return self.mana

    def set_mana(self, mana: int) -> None:
        """Sets the mana of the avatar.

        Parameters
        ----------
        `mana` : `int`
            The mana of the avatar.

        Returns
        -------
        `None`.
        """
        self.mana = mana

    def set_weapon(self, weapon: Wand) -> None:
        """Sets the weapon of the avatar.

        Parameters
        ----------
        `weapon` : `Wand`
            The weapon of the avatar.

        Returns
        -------
        `None`.
        """
        assert isinstance(weapon, Wand), "The given weapon must be a wand."
        return super().set_weapon(weapon)


class Warrior(Melee):
    """Class for defining warrior avatars.

    Class that defines a warrior avatar. It has a name, life, strength and
    defense. It can be equipped with a weapon (a `Sword`) and/or an armor.

    Attributes
    ----------
    `name` : `str`
        The name of the avatar.
    `life` : `int`
        The life of the avatar.
    `strength` : `int`
        The strength of the avatar.
    `defense` : `int`
        The defense of the avatar.
    `weapon` : `Weapon | None`
        The weapon of the avatar. Initially `None`.
    `armor` : `Armor | None`
        The armor of the avatar. Initially `None`.
    `shield` : `Shield | None`
        The shield of the avatar. Initially `None`.
    `fury` : `int`
        The fury of the avatar. Initially `0`.

    Methods
    -------
    `get_fury()`:
        Returns the fury of the avatar.
    `set_fury(fury: int)`:
        Sets the fury of the avatar.
    `attack()`:
        Returns the damage dealt by the avatar.
    `defend()`:
        Returns the damage blocked by the avatar.
    """

    def __init__(
        self, name: str, life: int, strength: int, defense: int, fury: int
    ) -> None:
        """Initializes the warrior avatar.

        Parameters
        ----------
        `name` : `str`
            The name of the avatar.
        `life` : `int`
            The life of the avatar.
        `strength` : `int`
            The strength of the avatar.
        `defense` : `int`
            The defense of the avatar.
        """
        super().__init__(name, life, strength, defense)

        self.fury = fury

    def get_fury(self) -> int:
        """Returns the fury of the avatar.

        Returns
        -------
        Fury of the avatar as an `int`.
        """
        return self.fury

    def set_fury(self, fury: int) -> None:
        """Sets the fury of the avatar.

        Parameters
        ----------
        `fury` : `int`
            The fury of the avatar.

        Returns
        -------
        `None`.
        """
        self.fury = fury

    def attack(self) -> int:
        """Returns the damage dealt by the avatar.

        Damage dealt by the avatar is the sum of its strength and the power of
        its weapon. If the avatar has fury, it will deal a random amount of
        damage between 0 and its fury.

        Returns
        -------
        Damage dealt by the avatar as an `int`.
        """
        return (
            self.strength + self.weapon.power
            if self.weapon
            else 0 + random.randint(0, self.fury)
        )

    def defend(self) -> int:
        """Returns the damage blocked by the avatar.

        Damage blocked by the avatar is the sum of its defense and the
        protection of its armor and shield.

        Returns
        -------
        Damage blocked by the avatar as an `int`.
        """
        return (
            self.defense + self.armor.protection
            if self.armor
            else 0 + self.shield.protection
            if self.shield
            else 0
        )


class Mage(Caster):
    """Class for defining mage avatars.

    Class that defines a mage avatar. It has a name, life, strength and
    defense. It can be equipped with a weapon (a `Wand`) and/or an armor.

    Attributes
    ----------
    `name` : `str`
        The name of the avatar.
    `life` : `int`
        The life of the avatar.
    `strength` : `int`
        The strength of the avatar.
    `defense` : `int`
        The defense of the avatar.
    `weapon` : `Weapon | None`
        The weapon of the avatar. Initially `None`.
    `armor` : `Armor | None`
        The armor of the avatar. Initially `None`.
    `mana` : `int`
        The mana of the avatar. Initially `0`.

    Methods
    -------
    `attack()`:
        Returns the damage dealt by the avatar.
    `defend()`:
        Returns the damage blocked by the avatar.
    """

    def __init__(
        self, name: str, life: int, strength: int, defense: int, mana: int
    ) -> None:
        """Initializes the mage avatar.

        Parameters
        ----------
        `name` : `str`
            The name of the avatar.
        `life` : `int`
            The life of the avatar.
        `strength` : `int`
            The strength of the avatar.
        `defense` : `int`
            The defense of the avatar.
        """
        super().__init__(name, life, strength, defense)

        self.mana = mana

    def attack(self) -> int:
        """Returns the damage dealt by the avatar.

        Damage dealt by the avatar is the sum of its strength and the power of
        its wand if it has enough mana.
        """
        if random.randint(0, 1):
            self.mana += 2

        weapon_damage = 1
        if self.weapon and self.mana > 1:
            weapon_damage = self.weapon.power

        self.mana -= 1

        if self.mana < 0:
            self.mana = 0

        return self.strength + weapon_damage

    def defend(self) -> int:
        """Returns the damage blocked by the avatar.

        Damage blocked by the avatar is the sum of its defense and the
        protection of its armor.

        Returns
        -------
        Damage blocked by the avatar as an `int`.
        """
        return self.defense + self.armor.protection if self.armor else 0


class Priest(Caster):
    """Class for defining priest avatars.

    Class that defines a priest avatar. It has a name, life, strength and
    defense. It can be equipped with a weapon (a `Wand`) and/or an armor.

    Attributes
    ----------
    `name` : `str`
        The name of the avatar.
    `life` : `int`
        The life of the avatar.
    `strength` : `int`
        The strength of the avatar.
    `defense` : `int`
        The defense of the avatar.
    `weapon` : `Weapon | None`
        The weapon of the avatar. Initially `None`.
    `armor` : `Armor | None`
        The armor of the avatar. Initially `None`.
    `mana` : `int`
        The mana of the avatar. Initially `0`.

    Methods
    -------
    `attack()`:
        Returns the damage dealt by the avatar.
    `defend()`:
        Returns the damage blocked by the avatar.
    """

    def __init__(
        self, name: str, life: int, strength: int, defense: int, mana: int
    ) -> None:
        """Initializes the Priest avatar.

        Parameters
        ----------
        `name` : `str`
            The name of the avatar.
        `life` : `int`
            The life of the avatar.
        `strength` : `int`
            The strength of the avatar.
        `defense` : `int`
            The defense of the avatar.
        """
        super().__init__(name, life, strength, defense)

        self.mana = mana

    def attack(self) -> int:
        """Returns the damage dealt by the avatar.

        Damage dealt by the avatar is the sum of its strength and the power of
        its wand if it has enough mana.
        """
        if random.randint(0, 1):
            self.mana += 2

        weapon_damage = 1
        if self.weapon and self.mana > 1:
            weapon_damage = self.weapon.power

        self.mana -= 1

        if self.mana < 0:
            self.mana = 0

        return self.strength + weapon_damage

    def defend(self) -> int:
        """Returns the damage blocked by the avatar.

        Damage blocked by the avatar is the sum of its defense and the
        protection of its armor.

        Returns
        -------
        Damage blocked by the avatar as an `int`.
        """
        return self.defense + self.armor.protection if self.armor else 0

    def heal(self) -> int:
        """Heals the avatar.

        Returns
        -------
        The amount of life healed as an `int`.
        """

        # Do nothing 50% of the time
        if random.randint(0, 1):
            return 0

        self.mana += 2

        healing = 0
        if self.mana > 2:
            healing += self.strength
            if self.weapon:
                healing += self.weapon.power

        healing = healing // 2
        self.mana -= 2

        self.set_life(self.life + healing)

        return healing
