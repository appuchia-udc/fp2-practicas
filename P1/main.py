#! /usr/bin/env python3

# Práctica 1 FP2: Simulaciones de combates
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

from argparse import ArgumentParser
from modules.avatars import Avatar, Warrior, Mage, Priest
from modules.simulation import simulate
from modules.statistics import Stats, calc_stats, display_stats
from settings import get_verboisty, set_verbosity, get_simulations, set_simulations


def run(path: str) -> None:
    """Parses the file and runs the simulation.

    Parameters
    ----------
    `path` : `str`
        Path to the file to parse PJs from.

    Returns
    -------
    `None`.
    """
    pjs = list()
    pj_stats = dict()

    with open(path) as f:
        lines = f.readlines()
        for pj in lines:
            pj_object = create_pj(pj.split())
            pjs.append(pj_object)
            pj_stats[pj_object.name] = Stats(
                pj_object.__class__.__name__, pj_object.name
            )

    for iteration in range(get_simulations()):
        alive = pjs.copy()

        # Run simulation and get winner
        winner = simulate(alive, pj_stats)
        if get_verboisty() > 0:
            print(
                f"\n[*] - El ganador de la iteración #{iteration + 1}/{get_simulations()} es: {winner}\n- - -"
            )

    # Display stats
    calculated_stats = calc_stats(list(pj_stats.values()))
    display_stats(*calculated_stats)


def create_pj(params: list[str]) -> Avatar:
    """Creates a PJ from the given parameters.

    Parameters
    ----------
    `params` : `list[str]`
        The list of parameters to use to parse the Avatar.

        Should be in the following format:
        [`type`, `name`, `life`, `strength`, `defense`, `fury`/`mana`]

    Returns
    -------
    `Warrior` or `Mage` object.
    """
    name, life, strength, protection = (
        params[1],
        int(params[2]),
        int(params[3]),
        int(params[4]),
    )
    if params[0].lower() == "warrior":
        fury = int(params[5])
        pj = Warrior(
            name=name,
            life=life,
            strength=strength,
            defense=protection,
            fury=fury,
        )
    elif params[0].lower() == "mage":
        mana = int(params[5])
        pj = Mage(
            name=name,
            life=life,
            strength=strength,
            defense=protection,
            mana=mana,
        )
    elif params[0].lower() == "priest":
        mana = int(params[5])
        pj = Priest(
            name=name,
            life=life,
            strength=strength,
            defense=protection,
            mana=mana,
        )
    else:
        raise ValueError(f"Avatar '{params[0]}' is not valid")

    if get_verboisty() > 2:
        print(f"[*] - Personaje creado: {pj}")

    return pj


if __name__ == "__main__":
    parser = ArgumentParser(description="Simulador de combates de personajes.")
    parser.add_argument(
        "path",
        type=str,
        help="Path to the file containing the PJs to simulate.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        help="Verbosity level (-v, -vv, -vvv)",
        default=0,
    )
    parser.add_argument(
        "-s",
        "--simulations",
        type=int,
        help="Number of simulations to run.",
        default=get_simulations(),
    )
    args = parser.parse_args()

    if args.verbose:
        set_verbosity(args.verbose)

    if args.simulations:
        set_simulations(args.simulations)

    run(args.path)
