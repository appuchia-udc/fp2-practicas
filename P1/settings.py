#! /usr/bin/env python3

# Práctica 1 FP2: Simulaciones de combates
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

"""
This file contains the settings for the simulations.
Constants are defined here.
"""

MAX_ITEM_VALUE = 5

_SIMULATIONS = 30
_VERBOSITY = 0


def get_verboisty() -> int:
    """Returns the verbosity level."""
    return _VERBOSITY


def set_verbosity(verbosity: int) -> None:
    """Sets the verbosity level."""
    global _VERBOSITY
    _VERBOSITY = verbosity


def set_simulations(simulations: int) -> None:
    """Sets the number of simulations to run."""
    global _SIMULATIONS
    _SIMULATIONS = simulations


def get_simulations() -> int:
    """Returns the number of simulations to run."""
    return _SIMULATIONS
