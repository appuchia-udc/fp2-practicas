# -*- coding: utf-8 -*-
# Copyright 2019, Profesorado de Fundamentos de Programación II
#                 Grado en Ciencia e Ingeneiría de Datos
#                 Facultade de Informática
#                 Universidade da Coruña
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Diferentes formas de crear iteradores
"""

def FuncionGeneradoraNumerosPares(size):
    """Ejemplo de función generadora (con yield en lugar de return)"""
    n = 0
    count = 1
    while count <= size:
        yield n
        n += 2
        count += 1

class ClaseNumerosPares1:
    """Ejemplo de iterable: una clase con métodos __len__ y __getitem__"""
    
    def __init__(self, size):
        self._size = size

    def __len__(self):
        """Devuelve la cantidad de números pares"""
        return self._size
    
    def __getitem__(self, k):
        """Devuelve la entrada de índice k (con la interperetación estándar si es negativo)"""
        if k < 0:
            k +=len(self)
            
        if not 0 <= k < self._size:
            raise IndexError("índice fuera de rango")
            
        return k * 2
    
class ClaseNumerosPares2:
    """Ejemplo de iterador: una clase con __next__ e __iter__"""
    
    def __init__(self, size):
        self._size = size
        self._count= -1
        
    def __next__(self):
        """Devuelve el siguiente elemento par o lanza la excepción StopIteration"""
        self._count += 1
        if self._count < self._size:
            return self._count * 2
        else:
            raise StopIteration()
            
    def __iter__(self):
        return self
        
    
class ClaseNumerosPares3:
    """Ejemplo de generador a partir de una clase con __iter__ y yield"""
    
    def __init__(self, size):
        self._size = size
        
    def __iter__(self):
        n = 0
        count = 1
        while count <= self._size:
            yield n
            n += 2
            count += 1
            
class ClaseNumerosPares4:
    """Ejemplo de clase con un método cualquiera que es un generador"""
      
    def generador(self, size):
        n = 0
        count = 1
        while count <= size:
            yield n
            n += 2
            count += 1
        
if __name__ == '__main__':

    print("Ejemplos de creación de iteradores (en este caso, de números pares)")
    print("1) Usando una función generadora", end=": ")
    for i in FuncionGeneradoraNumerosPares(10):
        print(i, end=" ")
    print()    
    par = FuncionGeneradoraNumerosPares(10)
    print("   a mano con 3 next:", next(par), next(par), next(par), "...")
    
    
    print("2) Usando una clase con __len__ y __getitem__", end=": ")
    for i in ClaseNumerosPares1(10):
        print(i, end=" ")
    print()
    pares = ClaseNumerosPares1(10)
    print("   accediendo al primer y último número par:", pares[0], pares[-1])
    par = iter(ClaseNumerosPares1(10))  # atención: se necesita llamar a iter
                                        # para crear el iterador
    print("   a mano con 3 next:", next(par), next(par), next(par), "...")
    
    
    print("3) Usando una clase con __next__ e __iter__", end=": ")
    for i in ClaseNumerosPares2(10):
        print(i, end=" ")
    print()
    par = ClaseNumerosPares2(10)    
    print("   a mano con 3 next:", next(par), next(par), next(par), "...")
    
    
    print("4) Usando una clase con __iter__ con yield", end=": ")
    for i in ClaseNumerosPares3(10):
        print(i, end=" ")
    print()
    par = iter(ClaseNumerosPares3(10))  # atención: se necesita llamar a iter
                                        # para crear el iterador
    print("   a mano con 3 next:", next(par), next(par), next(par), "...")
    
    print("5) Usando una clase con un método cualquiera que es un generador", end=": ")
    for i in ClaseNumerosPares4().generador(10):
        print(i, end=" ")
    print()
    par = ClaseNumerosPares4().generador(10) 
    print("   a mano con 3 next:", next(par), next(par), next(par), "...")  
    
    
    print("6) Usando Generator expressions", end=": ")
    for i in (x*2 for x in range(10)): # atención: no es una lista porque no va entre corchetes
        print(i, end=" ")
    print()
    par = (x*2 for x in range(10))
    print("   a mano con 3 next:", next(par), next(par), next(par), "...")  
    print()
