#! /usr/bin/env python3

"""TGR 2"""


class FibonacciNext:
    def __init__(self, cantidad):
        self.cantidad = cantidad

    def __iter__(self):
        self.n = 0
        self.a = 0
        self.b = 1
        return self

    def __next__(self):
        if self.n < self.cantidad:
            resultado = self.a
            self.a, self.b = self.b, self.a + self.b
            self.n += 1
            return resultado

        else:
            raise StopIteration


class FibonacciIter:
    def __init__(self, cantidad):
        self.cantidad = cantidad

    def __iter__(self):
        self.n = 0
        self.a = 0
        self.b = 1

        while self.n < self.cantidad:
            resultado = self.a
            self.a, self.b = self.b, self.a + self.b
            self.n += 1
            yield resultado


class FibonacciLen:
    def __init__(self, cantidad):
        self.cantidad = cantidad

    def __len__(self):
        return self.cantidad

    def __getitem__(self, indice):
        if indice >= self.cantidad:
            raise IndexError

        a = 0
        b = 1
        for _ in range(indice):
            a, b = b, a + b

        return a


def fib(cantidad):
    a = 0
    b = 1
    for _ in range(cantidad):
        yield a
        a, b = b, a + b


for i in FibonacciNext(20):
    print(i, end="  ")
print()

for i in FibonacciIter(20):
    print(i, end="  ")
print()

for i in FibonacciLen(20):
    print(i, end="  ")
print()

for i in fib(20):
    print(i, end="  ")
print()
