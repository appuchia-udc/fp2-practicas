from math import sqrt
from claseTriangulo import Triangulo

class TrianguloEq(Triangulo):
    """ Extensión de la clase Triángulo para manejar T. equiláteros """
 
    def __init__(self, a, b, c, x=0, y=0):
        if not self._valida_triangulo_equilatero(a, b, c):
            raise Exception("\t ERROR: Los lados no son iguales")            
        super().__init__('Triángulo Eq.', a, b, c, x, y)
        self._base = float(a)
        self._altura = sqrt(3*a)/2
    
    def _valida_triangulo_equilatero(self, a, b, c):
        return (a == b == c)
        
    def __str__(self):
        res = super().__str__()
        res += '\t Base = {0:4.2f} y Altura = {1:4.2f}\n'.format(self._x, self._y)
        return res  