from claseFigura import Figura

class Rombo(Figura):
    """ Extensión de la clase Figura para manejar Rombos """
     
    def __init__(self, d=1, D=1, x=0, y=0):
        super().__init__('Rombo', x, y)
        self._diagonal_menor = d
        self._diagonal_mayor = D
        
    def get_diagonal_menor(self):
        """ Devuelve la diagonal menor del rombo """
        return self._diagonal_menor
    
    def get_diagonal_mayor(self):
        """ Devuelve la diagonal mayor del rombo """
        return self._diagonal_mayor

    def perimetro(self):
        """ Calcula el perímetro del rombo """
        from math import sqrt
        return 2 * sqrt(self._diagonal_mayor ** 2 + self._diagonal_menor ** 2)
    
    def area(self):
        """ Calcula el área del rombo """        
        return self._diagonal_mayor * self._diagonal_menor / 2

    def __str__(self):
        res = self.get_nombre()
        res += '\n\t Coordenadas x = {0}, y = {1}\n'.format(self._x, self._y)
        res += '\t Área = {0:4.2f}\n'.format(self.area())
        return res 