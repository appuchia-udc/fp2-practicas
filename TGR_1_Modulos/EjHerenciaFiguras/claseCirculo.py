from claseFigura import Figura

class Circulo(Figura):
    """ Extensión de la clase Figura para manejar Círculos """

    def __init__(self, radio=1, x=0, y=0):
        self.set_radio(radio)
        super().__init__('Círculo', x, y)

    def set_radio(self, radio):
        """ Establece el valor del radio del círculo"""
        if radio <= 0:
            raise ValueError("El valor del 'radio' es negativo o cero")
        self._radio = radio
        self._diametro = 2 * radio
        
    def get_radio(self):
        """ Devuelve el radio del círculo """
        return self._radio
    
    def get_diametro(self):
        """ Devuelve el diámetro del círculo """
        return self._diametro

    def longitud(self):
        """ Calcula la longitud de la circunferencia """
        from math import pi
        return 2 * pi * self._radio
    
    def perimetro(self):
        return self.get_longitud()
    
    def area(self):
        """ Calcula el área del círculo """
        from math import pi
        return pi * (self._radio ** 2)
    
    def __str__(self):
        res = self.get_nombre()
        res += '\n\t Coordenadas x = {0}, y = {1}\n'.format(self._x, self._y)
        res += '\t Área = {0:4.2f}\n'.format(self.area())
        return res   