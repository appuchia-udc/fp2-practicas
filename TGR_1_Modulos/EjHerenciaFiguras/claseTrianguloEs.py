from claseTriangulo import Triangulo

class TrianguloEs(Triangulo):
    """ Extensión de la clase Triángulo para manejar T. escalenos """

    def __init__(self, a, b, c, x=0, y=0):
        super().__init__('Triángulo Es.', a, b, c, x, y)
        self._base = max([a, b, c])
        self._altura = self.calcular_altura() 

    def calcular_altura(self):
        s = (self._a + self._b + self._c) / 2
        if self._base == self._a:
            return (2 / self._a) * ((s * (s-self._a) * (s-self._b) * (s-self._c)) ** 0.5)
        if self._base == self._b:
            return (2 / self._b) * ((s * (s-self._a) * (s-self._b) * (s-self._c)) ** 0.5)
        if self._base == self._c:
            return (2 / self._c) * ((s * (s-self._a) * (s-self._b) * (s-self._c)) ** 0.5)