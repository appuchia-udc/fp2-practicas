from claseFigura import Figura

class Trapecio(Figura):
    """ Extensión de la clase Figura para manejar Trapecios """
     
    def __init__(self, a=10, b=4, c=5, d=5, x=0, y=0):
        super().__init__('Trapecio', x, y)
        self._a = a
        self._b = b
        self._c = c
        self._d = d
        self._altura = self.get_altura()
        
    def get_altura(self):
        """ Calcula la altura del trapecio """
        return 2* self.area() / (self._a + self._b)
    
    def perimetro(self):
        """ Calcula el perímetro del trapecio """
        return self._a + self._b + self._c + self._d
    
    def area(self):
        """ Calcula el área del trapecio """
        from math import sqrt
        return (self._a + self._b) / 2 * sqrt(self._c**2 - ((self._c**2 - self._d**2 + (self._a - self._b)**2)/(2*(self._a - self._b))) ** 2)

    def __str__(self):
        res = self.get_nombre()
        res += '\n\t Coordenadas x = {0}, y = {1}\n'.format(self._x, self._y)
        res += '\t Área = {0:4.2f}\n'.format(self.area())
        return res 