from claseFigura import Figura

class Cuadrado(Figura):
    """ Extensión de la clase Figura para manejar Cuadrados"""
     
    def __init__(self, lado=1, x=0, y=0):
        super().__init__('Cuadrado', x, y)
        self._lado = lado
        
    def get_lado(self):
        """ Devuelve el lado del cuadrado """
        return self._lado
    
    def perimetro(self):
        """ Calcula el perímetro del cuadrado """
        return 4 * self._lado
    
    def area(self):
        """ Calcula el área del cuadrado """        
        return self._lado ** 2

    def __str__(self):
        res = self.get_nombre()
        res += '\n\t Coordenadas x = {0}, y = {1}\n'.format(self._x, self._y)
        res += '\t Área = {0:4.2f}\n'.format(self.area())
        return res 