class Figura:
    def __init__(self, nombre, x, y):
        self._nombre = nombre
        self._x = x
        self._y = y
        
    def get_coordenadas(self):
        """ Devuelve las coordenadas x e y """
        return self._x, self._y
    
    def get_nombre(self):
        """ Devuelve el tipo de figura """
        return self._nombre
    
    def mover(self, delta_x, delta_y):
        """ Desplaza la posición x e y de la figura """
        self.x = self.x + delta_x
        self.y = self.y + delta_y

    def area(self):
        """ Calcula el área de la figura """
        raise Exception("Las subclases sobreescriben este método.")
        
    def perimetro(self):
        """ Calcula el perímetro de la figura """
        raise Exception("Las subclases sobreescriben este método.")
        
    def __str__(self):
        res = 'Figura indefinida:\n'
        res += '\t Coordenadas x = {0}, y = {1}\n'.format(self._x, self._y)
        return res            