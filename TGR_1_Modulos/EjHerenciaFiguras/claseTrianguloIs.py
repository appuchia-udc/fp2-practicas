from math import sqrt
from claseTriangulo import Triangulo

class TrianguloIs(Triangulo):
    """ Extensión de la clase Triángulo para manejar T. isósceles """

    def __init__(self, a, b, c, x=0, y=0):
        if not self._valida_triangulo_isosceles(a, b, c):    
             raise Exception("\t ERROR: Sólo dos lados deben ser iguales")            
        super().__init__('Triángulo Is.', a, b, c, x, y)
        lados = sorted([self._a, self._b, self._c]) 
        self._base = float(lados[0])
        self._altura = sqrt(lados[2]**2 - (lados[1]**2/4))
        
    def _valida_triangulo_isosceles(self, a, b, c):
        iguales = 0
        if (a == b): iguales +=1
        if (a == c): iguales +=1
        if (c == b): iguales +=1
        return (iguales == 1)
    
    def __str__(self):
        res = super().__str__()
        res += '\t Base = {0:4.2f} y Altura = {1:4.2f}\n'.format(self._base, self._altura)
        return res    