from claseFigura import Figura

class Triangulo(Figura):
    """ Extensión de la clase Figura para manejar Triángulos """
 
    def __init__(self, tipo, a, b, c, x, y):
        self._validar_lados(a, b, c)
        super().__init__(tipo, x, y)
        self._a = a
        self._b = b
        self._c = c
      
    def _validar_lados(self, a, b, c):
        """ Comprobar si lados cumplen la desigualdad triangular """
        if     ((a + b < c) 
            or (a + c < b)
            or (c + b < a)):
            raise Exception("\t ERROR: Los lados no cumplen la desigualdad triangular")
        
    def get_base(self):
        """ Devuelve la base del triángulo """
        return self._base
    
    def get_altura(self):
        """ Devuelve la altura del triángulo """
        return self._altura
    
    def perimetro(self):
        """ Calcula el perímetro del triángulo """
        return self._a + self._b + self._c

    def area(self):
        """ Fórmula de Herón """
        s = (self._a + self._b + self._c) / 2
        return (s * (s-self._a) * (s-self._b) * (s-self._c)) ** 0.5
    
    def __str__(self):
        res = self.get_nombre()
        res += '\n\t Coordenadas x = {0}, y = {1}\n'.format(self._x, self._y)
        res += '\t Área = {0:4.2f}\n'.format(self.area())
        return res    