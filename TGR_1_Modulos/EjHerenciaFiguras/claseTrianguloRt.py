from math import sqrt
from claseTriangulo import Triangulo

class TrianguloRt(Triangulo):
    """ Extensión de la clase Triángulo para manejar T. rectángulos """

    def __init__(self, a, b, c, x=0, y=0):
        lados = sorted([a, b, c])
        if not self._valida_triangulo_rectangulo(lados):
            raise Exception("\t ERROR: No cumple el teorema de Pitágoras")            
        super().__init__('Triángulo Rt.', a, b, c, x, y)
        lados.sort()
        self._base = lados[0]
        self._altura = lados[1]
    
    def get_hipotenusa(self):
        return self._hipotenusa
    
    def _valida_triangulo_rectangulo(self, lados):
        """ Valida si es tr. rectángulo con teor. Pitágoras """
        self._hipotenusa = lados[2]
        self._cateto1 = lados[0]
        self._cateto2 = lados[1]
        return self._hipotenusa == round(sqrt(self._cateto1**2 + self._cateto2**2), 2)
    
    def __str__(self):
        res = super().__str__()
        res += '\t Cateto1  ={0:4.2f} y Cateto2 = {1:4.2f}\n'.format(self._cateto1, self._cateto2)
        return res    