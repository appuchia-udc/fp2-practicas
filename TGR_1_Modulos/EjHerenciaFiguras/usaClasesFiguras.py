# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 16:19:14 2020

@author: Mariano
"""
from claseCuadrado import Cuadrado
from claseTrianguloIs import TrianguloIs
from claseTrianguloEq import TrianguloEq
from claseTrianguloRt import TrianguloRt
from claseTrianguloEs import TrianguloEs
from claseCirculo import Circulo
from claseTrapecio import Trapecio
from claseRombo import Rombo
from claseFigura import Figura


if __name__ == '__main__':
    figuras = []
    figuras.append(Cuadrado(3, 5, 7))
    figuras.append(Circulo(2, 7, 10))
    figuras.append(TrianguloIs(2, 4, 2, 1, 1))
    figuras.append(TrianguloEq(2, 2, 2, 1, 1))
    figuras.append(TrianguloRt(4, 5, 3, 1, 1))
    figuras.append(TrianguloEs(9, 5, 6, 1, 1))
    figuras.append(Rombo(5, 8))
    figuras.append(Trapecio(7, 4, 4.47, 4.12))
    figuras.append(Figura('Sin nombre', 0, 0))
    
    for item in figuras:
        print(item)
    
    # Métodos que acceden a propiedades calculadas por las clases hijas
    triangulo = TrianguloIs(4, 3, 4)
    print("Triángulo isósceles (4,3,4)")
    print("\t Base {0:4.2f} y altura {1:4.2f}\n". format(triangulo.get_base(), triangulo.get_altura()))

    # Métodos sobrescritos por las clases hijas
    triangulo = TrianguloEs(5, 9, 6)
    print("Triángulo escaleno (5,9,6)")
    print("\t Base {0:4.2f} y altura {1:4.2f}\n". format(triangulo.get_base(), triangulo.get_altura()))
    
    # Triángulos imposibles
    try:
        triangulo = TrianguloEq(3, 3, 4)
    except Exception as e:
        print("Triángulo equilátero (3,3,4) {}".format(e))

    try:
        triangulo = TrianguloIs(3, 6, 9)
    except Exception as e:
        print("Triángulo isósceles 3,6,9 {}".format(e))
    
    try:
        triangulo = TrianguloEs(3, 5, 9)
    except Exception as e:
        print("Triángulo escaleno 3,5,9 {}".format(e))
        
    try:        
        triangulo = TrianguloRt(5, 5.66, 4)
    except Exception as e:
        print("Triángulo rectángulo 4,5.66,4 {}".format(e))
    
