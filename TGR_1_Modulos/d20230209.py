#! /usr/bin/env python3

from typing import Iterable, Any


def count(data: Iterable[Any], target: Any) -> int:
    n = 0
    for item in data:
        if item == target:
            n += 1
    return n


def better_count(data: Iterable[Any], target: Any) -> int:
    return sum(1 for item in data if item == target)


def other_count(data: Iterable[Any], target: Any) -> int:
    return len(list(filter(lambda x: x == target, data)))


grades = ["A", "B+", "A"]
result = count(grades, "A")


from math import pi as PI

pi = 3.14
print(pi, PI)

from d20230209_modules import *

print(var1, var2, var3)
f1()

from EjHerenciaFiguras.claseCirculo import Circulo as C

c = C(3)
print(c.area())

from EjHerenciaFiguras.claseCuadrado import Cuadrado as Q

q = Q(3)
print(q.area())
