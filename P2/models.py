#! /usr/bin/env python3

# Práctica 2 FP2: Colas de despegue
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>


# Plane
class Plane:
    """Class that represents a plane.

    A plane has an id, a runway time, a classification and a priority.

    Attributes
    ----------
    plane_id: str
        The plane's id.
    runway_time: int
        The time the plane landed.
    classification: str
        The plane's classification.
    priority: int
        The plane's priority.

    Methods
    -------
    wait_time(current_time: int) -> int
        Returns the time the plane has been waiting in the queue.
    """

    def __init__(
        self, plane_id: str, runway_time: int, classification: str, priority: int
    ):
        self.plane_id = plane_id
        self.runway_time = runway_time
        self.classification = classification
        self.priority = priority

    def __str__(self):
        return f"Plane {self.plane_id} with runway time {self.runway_time} and priority {self.priority}"

    def wait_time(self, current_time: int) -> int:
        """Return the time the plane has been waiting in the queue.

        Parameters
        ----------
        current_time: int
            The current time.

        Returns
        -------
        int
            The time the plane has been waiting in the queue.
        """

        return current_time - self.runway_time


# Queue
class PlaneQueue:
    """FIFO queue implementation using a Python list as underlying storage. Stores planes.

    Methods
    -------
    is_empty() -> bool
        Return True if the queue is empty.
    first() -> Plane | None
        Return (but do not remove) the element at the front of the queue.
    enqueue(plane: Plane) -> None
        Add an element to the back of queue.
    dequeue() -> Plane | None
        Remove and return the first element of the queue.
    """

    def __init__(self):
        """Create an empty queue."""

        self._data = list()

    def __len__(self) -> int:
        """Return the number of elements in the queue."""

        return len(self._data)

    def __str__(self) -> str:
        """A string representation of the queue."""

        if not self.is_empty():
            return f"Queue of size {len(self._data)} with first element {self.first()}"

        return "Empty queue"

    def is_empty(self) -> bool:
        """Return True if the queue is empty.

        Returns
        -------
        bool
            True if the queue is empty, False otherwise.
        """

        return len(self._data) == 0

    def first(self) -> Plane | None:
        """Return (but do not remove) the element at the front of the queue.

        Returns None if the queue is empty.

        Returns
        -------
        Plane
            The first plane in the queue.
        """

        if self.is_empty():
            return None

        return self._data[0]

    def enqueue(self, plane: Plane) -> None:
        """Add an element to the back of queue.

        Parameters
        ----------
        plane: Plane
            The plane to add to the queue.

        Returns
        -------
        None
        """

        self._data.append(plane)

    def dequeue(self, idx: int = 0) -> Plane | None:
        """Remove and return the first element of the queue.

        Returns None if the queue is empty.

        Returns
        -------
        Plane
            The plane that was removed from the queue.
        """

        if self.is_empty():
            return None

        return self._data.pop(idx)
