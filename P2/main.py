#! /usr/bin/env python3

# Práctica 2 FP2: Colas de despegue
# authors: Pablo Fdez <p.fernandezf@udc.es>, Brais García <brais.garcia2@udc.es>

import sys

from models import Plane, PlaneQueue

FLIGHT_PRIORITIES = {
    "domestico": 1,
    "privado": 2,
    "regular": 3,
    "charter": 4,
    "transoceanico": 5,
}
MAX_WAIT_TIME = 20  # Time to wait before increasing a plane's priority
TAKEOFF_INTERVAL = 5  # Time between takeoffs


def run(path):
    current_time = 0
    runways = [PlaneQueue() for _ in FLIGHT_PRIORITIES]
    schedule = PlaneQueue()

    # Create flight schedule
    with open(path) as f:
        for line in f.readlines():
            runway_time, flight_id, flight_type = line.strip().split(" ")
            schedule.enqueue(
                Plane(
                    plane_id=flight_id,
                    runway_time=int(runway_time),
                    classification=flight_type,
                    priority=FLIGHT_PRIORITIES.get(flight_type, len(FLIGHT_PRIORITIES)),
                )
            )

    # Loop while runways or schedule have planes
    while any([not runway.is_empty() for runway in runways]) or not schedule.is_empty():
        print(f"[·] Tiempo actual: {current_time}")

        # Add planes to the runways
        while (plane := schedule.first()) and plane.runway_time == current_time:
            schedule.dequeue()
            runways[plane.priority - 1].enqueue(plane)
            print(
                f"Entrando a pista vuelo... {plane.plane_id} {plane.classification} {current_time}"
            )

        # Take off first plane in highest priority runway with planes
        if current_time % TAKEOFF_INTERVAL == 0:
            for runway in runways:
                if plane := runway.dequeue():
                    print(
                        f"Despegando vuelo... {plane.plane_id} {plane.classification} {plane.runway_time} {current_time}"
                    )
                    break

        # Increase priority of planes that have been waiting for too long (except highest priority)
        for runway in runways[1:]:
            while (plane := runway.first()) and plane.wait_time(
                current_time
            ) > MAX_WAIT_TIME:
                runway.dequeue()
                plane.priority -= 1  # Lower number means higher priority
                plane.runway_time = current_time
                runways[plane.priority - 1].enqueue(plane)

        # Tick clock
        current_time += 1

    print()
    print("Todos los vuelos han despegado.")


if __name__ == "__main__":
    run(sys.argv[1])
